# README #

Infinispan Demo Webapp

### What is this repository for? ###

* Vaadin web application demonstrating basic Infinispan functionality including clustering and distributed map/reduce task execution
* Version 0.3-SNAPSHOT as current dev brunch, version 0.2 - current stable 

### How do I get set up? ###

  Requirements 
-------------
   * Java 7+
   * Maven 3+

  Configuration options:
-------------
   * -Dtrade.part={one, two} - load first or second part of sample objects set into node, by default it loads all objects 
   *  -Djgroups.transport.profile={udp, ec2} - classic (localhost, LAN) or AWS EC2 profiles 
   *  -Djgroups.tcp.address={IP_ADDRESS} - bind address
   *  -Djgroups.tcpping.initial_hosts={IP_ADDRESS1[PORT],IP_ADDRESS2[PORT]...} - EC2 cluster nodes addresses and port numbers   
   *  -Djava.net.preferIPv4Stack=true - bind to IPv4 address by default

Deployment instructions
-------------
You can deploy application on any modern Java Servlet Container or Application Server by standard deployment of WAR file. Add appropriate configuration parameters to server startup script if needed.

### Who do I talk to? ###

* Anton Kozik (givenname.lastname at Gmail)