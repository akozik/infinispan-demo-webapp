package pl.akozik.infinispan.demo;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;

import org.infinispan.Cache;
import org.infinispan.distexec.mapreduce.Collector;
import org.infinispan.distexec.mapreduce.MapReduceTask;
import org.infinispan.distexec.mapreduce.Mapper;
import org.infinispan.distexec.mapreduce.Reducer;

import pl.akozik.infinispan.demo.cache.CacheManager;
import pl.akozik.infinispan.demo.trade.Trade;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;

/**
 * 
 * MapReduce button click action listener.
 * 
 * @author Anton Kozik
 * 
 */
class MapReduceButtonClickListener implements Button.ClickListener {

    private static final long serialVersionUID = -8501562789951448944L;

    /**
     * Executes MapReduce tasks on a cache.
     */
    @Override
    public void buttonClick(final ClickEvent event) {
	Cache<Integer, Trade> cache = CacheManager.getInstance().getCache(CacheManager.CACHE_NAME);

	final MapReduceTask<Integer, Trade, String, Integer> countryAggregationTask = new MapReduceTask<>(cache);
	countryAggregationTask.mappedWith(new CountryMapper()).reducedWith(new CountryReducer());

	final MapReduceTask<Integer, Trade, String, BigDecimal> countrySumTask = new MapReduceTask<>(cache);
	countrySumTask.mappedWith(new CountrySumMapper()).reducedWith(new CountrySumReducer());

	final MapReduceTask<Integer, Trade, String, BigDecimal> productSumTask = new MapReduceTask<>(cache);
	productSumTask.mappedWith(new ProductSumMapper()).reducedWith(new ProductSumReducer());

	final long start = System.nanoTime();

	final Map<String, Integer> results = countryAggregationTask.execute();
	final Map<String, BigDecimal> resultsOfCountrySum = countrySumTask.execute();
	final Map<String, BigDecimal> resultOfProductSum = productSumTask.execute();

	final long finish = System.nanoTime();
	final int timing = Math.round((finish - start) / 1_000_000);
	Notification.show("MapReduce task execution took " + timing + " ms");
	updateOutput(results, resultsOfCountrySum, resultOfProductSum);
	updateLastMapReduceTime(timing);
    }

    private void updateLastMapReduceTime(final int timing) {
	((InfinispanDemoUI) UI.getCurrent()).getMapReduceTimeLabel().setValue(
		String.format(InfinispanDemoUI.MAP_REDUCE_LABEL_FORMAT, timing));
    }

    private void updateOutput(final Map<String, Integer> results, final Map<String, BigDecimal> resultsOfCountrySum,
	    final Map<String, BigDecimal> resultOfProductSum) {
	final Table output = ((InfinispanDemoUI) UI.getCurrent()).getCountryNumberOfOrdersTable();
	output.removeAllItems();
	for (final Map.Entry<String, Integer> result : results.entrySet()) {
	    output.addItem(new Object[] { result.getKey(), result.getValue() }, result.getKey());
	}

	final Table countrySumTable = ((InfinispanDemoUI) UI.getCurrent()).getCountrySumTable();
	countrySumTable.removeAllItems();
	for (final Map.Entry<String, BigDecimal> result : resultsOfCountrySum.entrySet()) {
	    countrySumTable.addItem(new Object[] { result.getKey(), result.getValue() }, result.getKey());
	}

	final Table productSumTable = ((InfinispanDemoUI) UI.getCurrent()).getProductSumTable();
	productSumTable.removeAllItems();
	for (final Map.Entry<String, BigDecimal> result : resultOfProductSum.entrySet()) {
	    productSumTable.addItem(new Object[] { result.getKey(), result.getValue() }, result.getKey());
	}
    }

    /**
     * 
     * Country mapper.
     * 
     * @author Anton Kozik
     * 
     */
    class CountryMapper implements Mapper<Integer, Trade, String, Integer> {

	private static final long serialVersionUID = -8650259261622322594L;

	/**
	 * Maps country from trade.
	 */
	@Override
	public void map(final Integer key, final Trade value, final Collector<String, Integer> collector) {
	    final String country = value.getCountry();
	    collector.emit(country, Integer.valueOf(1));
	}

    }

    /**
     * 
     * Country reducers.
     * 
     * @author Anton Kozik
     * 
     */
    class CountryReducer implements Reducer<String, Integer> {

	private static final long serialVersionUID = 5094756655528750775L;

	/**
	 * Counts up all the country occurrence.
	 */
	@Override
	public Integer reduce(final String reducedKey, final Iterator<Integer> iter) {
	    int sum = 0;
	    while (iter.hasNext()) {
		sum += iter.next();
	    }
	    return Integer.valueOf(sum);
	}

    }

    /**
     * 
     * Amount per country mapper.
     * 
     * @author Anton Kozik
     * 
     */
    class CountrySumMapper implements Mapper<Integer, Trade, String, BigDecimal> {

	private static final long serialVersionUID = -3649215195450917353L;

	/**
	 * Emits country and amount of some trade.
	 */
	@Override
	public void map(final Integer key, final Trade value, final Collector<String, BigDecimal> collector) {
	    final String country = value.getCountry();
	    final BigDecimal amount = value.getAmount();
	    collector.emit(country, amount);
	}

    }

    /**
     * 
     * Amount per country reducer.
     * 
     * @author Anton Kozik
     * 
     */
    class CountrySumReducer implements Reducer<String, BigDecimal> {

	private static final long serialVersionUID = 6599073597146241098L;

	/**
	 * Sums up all amount per country.
	 */
	@Override
	public BigDecimal reduce(final String country, final Iterator<BigDecimal> iter) {
	    BigDecimal sum = BigDecimal.ZERO;
	    while (iter.hasNext()) {
		sum = sum.add(iter.next());
	    }
	    return sum;
	}

    }

    /**
     * 
     * Amount per product mapper.
     * 
     * @author Anton Kozik
     * 
     */
    class ProductSumMapper implements Mapper<Integer, Trade, String, BigDecimal> {

	private static final long serialVersionUID = -3649215195450917353L;

	/**
	 * Emits product and amount of trade.
	 */
	@Override
	public void map(final Integer key, final Trade value, final Collector<String, BigDecimal> collector) {
	    final String product = value.getProduct();
	    final BigDecimal amount = value.getAmount();
	    collector.emit(product, amount);
	}

    }

    /**
     * 
     * Sum of product sales.
     * 
     * @author Anton Kozik
     * 
     */
    class ProductSumReducer implements Reducer<String, BigDecimal> {

	private static final long serialVersionUID = 6599073597146241098L;

	/**
	 * Sums up per product sales.
	 */
	@Override
	public BigDecimal reduce(final String product, final Iterator<BigDecimal> iter) {
	    BigDecimal sum = BigDecimal.ZERO;
	    while (iter.hasNext()) {
		sum = sum.add(iter.next());
	    }
	    return sum;
	}

    }

}
