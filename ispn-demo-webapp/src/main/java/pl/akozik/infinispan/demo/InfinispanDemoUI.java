package pl.akozik.infinispan.demo;

import static java.lang.String.format;

import java.math.BigDecimal;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import pl.akozik.infinispan.demo.cache.CacheManager;
import pl.akozik.infinispan.demo.trade.Trade;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * 
 * Infinispan Web Application main class. Creates UI with all elements.
 * 
 * @author Anton Kozik
 * 
 */
@Theme("moloko")
@Title("Infinispan Demo")
public class InfinispanDemoUI extends UI {

    /**
     * 
     * Simple Vaadin configured servlet configuration
     * 
     * @author Anton Kozik
     * 
     */
    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = InfinispanDemoUI.class, widgetset = "pl.akozik.infinispan.demo.InfinispanDemoWidgetSet")
    public static class Servlet extends VaadinServlet {

	private static final long serialVersionUID = -4772168851929643248L;

    }

    /**
     * 
     * Servlet listener that starts and stops Infinispan cache on application
     * load/shutdown.
     * 
     * @author Anton Kozik
     * 
     */
    @WebListener
    public static class InfinispanCacheListener implements ServletContextListener {

	/**
	 * Start up a cache.
	 */
	@Override
	public void contextInitialized(final ServletContextEvent servletContextEvent) {
	    // initializes cache
	    CacheManager.getInstance().getCache(CacheManager.CACHE_NAME);
	}

	/**
	 * Stop Infinispan.
	 */
	@Override
	public void contextDestroyed(final ServletContextEvent servletContextEvent) {
	    CacheManager.stop();
	}

    }

    /** Number of cache entries label format string **/
    public static final String ADD_LABEL_FORMAT = "Number of objects : %d";

    /** MapReduce label format string **/
    public static final String MAP_REDUCE_LABEL_FORMAT = "MapReduce task timing: %d ms";

    private static final long serialVersionUID = -4494371955020533219L;

    private static final float TABLE_HEIGHT = 400f;

    private static final float BUTTON_WIDTH = 220f;

    private final Label numberOfEntriesLabel = new Label(format(ADD_LABEL_FORMAT, 0));

    private final Label mapReduceTimeLabel = new Label(format(MAP_REDUCE_LABEL_FORMAT, -1));

    private final Table dataTable = new Table();

    private final Table countryNumberOfOrdersTable = new Table();

    private final Table countrySumTable = new Table();

    private final Table productSumTable = new Table();

    /**
     * Main method
     */
    @Override
    protected void init(final VaadinRequest request) {
	final HorizontalSplitPanel splitLayoutPanel = new HorizontalSplitPanel();
	final VerticalLayout layout = new VerticalLayout();
	layout.setMargin(true);
	layout.setSpacing(true);
	splitLayoutPanel.setFirstComponent(layout);

	setContent(splitLayoutPanel);

	final Label actions = new Label("Actions");
	actions.setStyleName(Reindeer.LABEL_H1);
	layout.addComponent(actions);

	final Button addButton = new Button("Add objects to cache");
	addButton.addClickListener(new AddButtonClickListener());
	addButton.setWidth(BUTTON_WIDTH, Unit.PIXELS);
	layout.addComponent(addButton);

	final Button mapReduceButton = new Button("Execute map/reduce task");
	mapReduceButton.addClickListener(new MapReduceButtonClickListener());
	mapReduceButton.setWidth(BUTTON_WIDTH, Unit.PIXELS);
	layout.addComponent(mapReduceButton);

	final Button refreshButton = new Button("Refresh");
	refreshButton.addClickListener(new RefreshButtonClickListener());
	refreshButton.setWidth(BUTTON_WIDTH, Unit.PIXELS);
	layout.addComponent(refreshButton);

	final Button clearCacheButton = new Button("Clear cache");
	clearCacheButton.addClickListener(new ClearCacheButtonClickListener());
	clearCacheButton.setWidth(BUTTON_WIDTH, Unit.PIXELS);
	layout.addComponent(clearCacheButton);

	final VerticalLayout statusLayout = new VerticalLayout();
	statusLayout.setSpacing(true);
	statusLayout.setMargin(true);

	final Label status = new Label("Status");
	status.setStyleName(Reindeer.LABEL_H1);
	statusLayout.addComponent(status);

	final Panel panel = new Panel();
	panel.setSizeFull();

	final VerticalLayout statsLayout = new VerticalLayout();
	statsLayout.setSpacing(true);
	statsLayout.setMargin(true);
	statsLayout.addComponent(numberOfEntriesLabel);

	final BeanItemContainer<Trade> tradeDataContainer = new BeanItemContainer<>(Trade.class);
	dataTable.setContainerDataSource(tradeDataContainer);
	dataTable.setHeight(TABLE_HEIGHT, Unit.PIXELS);
	dataTable.setWidth(100, Unit.PERCENTAGE);
	dataTable.setColumnReorderingAllowed(true);
	statsLayout.addComponent(dataTable);

	statsLayout.addComponent(mapReduceTimeLabel);

	final HorizontalLayout outputLayout = new HorizontalLayout();
	outputLayout.setSpacing(true);

	countryNumberOfOrdersTable.addContainerProperty("Country", String.class, null);
	countryNumberOfOrdersTable.addContainerProperty("Number of orders", Integer.class, Integer.valueOf(0));
	countryNumberOfOrdersTable.setHeight(300f, Unit.PIXELS);
	outputLayout.addComponent(countryNumberOfOrdersTable);

	countrySumTable.addContainerProperty("Country", String.class, null);
	countrySumTable.addContainerProperty("Sum", BigDecimal.class, BigDecimal.ZERO);
	countrySumTable.setHeight(300f, Unit.PIXELS);
	outputLayout.addComponent(countrySumTable);

	productSumTable.addContainerProperty("Product", String.class, null);
	productSumTable.addContainerProperty("Sum", BigDecimal.class, BigDecimal.ZERO);
	productSumTable.setHeight(300f, Unit.PIXELS);
	outputLayout.addComponent(productSumTable);

	statsLayout.addComponent(outputLayout);
	panel.setContent(statsLayout);

	statusLayout.addComponent(panel);

	splitLayoutPanel.setSecondComponent(statusLayout);
	splitLayoutPanel.setSplitPosition(300f, Unit.PIXELS);
    }

    /**
     * Updates number of cache entries.
     * 
     * @param numberOfEntries
     */
    public void updateNumberOfEntriesLabel(final int numberOfEntries) {
	numberOfEntriesLabel.setValue(format(ADD_LABEL_FORMAT, numberOfEntries));
    }

    /**
     * Number of entries label getter
     * 
     * @return label object
     */
    public Label getNumberOfEntriesLabel() {
	return numberOfEntriesLabel;
    }

    /**
     * MapReduce time label getter
     * 
     * @return mapreduce timing label
     */
    public Label getMapReduceTimeLabel() {
	return mapReduceTimeLabel;
    }

    /**
     * Number of orders per country table
     * 
     * @return number of orders per country table
     */
    public Table getCountryNumberOfOrdersTable() {
	return countryNumberOfOrdersTable;
    }

    /**
     * 
     * 
     * 
     * @return overall amount of trades per country table
     */
    public Table getCountrySumTable() {
	return countrySumTable;
    }

    /**
     * 
     * Sum of all trades per product table.
     * 
     * @return overall amount of trades per product table
     */
    public Table getProductSumTable() {
	return productSumTable;
    }

    /**
     * 
     * Table with trades.
     * 
     * @return trade table
     */
    public Table getDataTable() {
	return dataTable;
    }

}
