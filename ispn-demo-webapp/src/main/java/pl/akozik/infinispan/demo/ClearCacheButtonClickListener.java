package pl.akozik.infinispan.demo;

import static pl.akozik.infinispan.demo.cache.CacheManager.CACHE_NAME;

import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;

import pl.akozik.infinispan.demo.cache.CacheManager;
import pl.akozik.infinispan.demo.trade.Trade;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

/**
 * 
 * Clear cache button click actioni implementation
 * 
 * @author Anton Kozik
 * 
 */
class ClearCacheButtonClickListener extends RefreshButtonClickListener {

    private static final long serialVersionUID = 4448359470605690543L;

    /**
     * Clears cache
     */
    @Override
    public void buttonClick(final ClickEvent event) {
	final EmbeddedCacheManager manager = CacheManager.getInstance();
	final Cache<Integer, Trade> cache = manager.getCache(CACHE_NAME);
	cache.clear();
	Notification.show("All objects are removed from cache!");
	clearTables();
	super.buttonClick(event);
    }

    private void clearTables() {
	((InfinispanDemoUI) UI.getCurrent()).getDataTable().removeAllItems();
	((InfinispanDemoUI) UI.getCurrent()).getCountryNumberOfOrdersTable().removeAllItems();
	((InfinispanDemoUI) UI.getCurrent()).getCountrySumTable().removeAllItems();
	((InfinispanDemoUI) UI.getCurrent()).getProductSumTable().removeAllItems();
    }

}
