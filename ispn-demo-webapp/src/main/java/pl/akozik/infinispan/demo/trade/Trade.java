package pl.akozik.infinispan.demo.trade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * Trade entity. An object representing simple trade.
 * 
 * @author Anton Kozik
 * 
 */
public class Trade implements Serializable {

    private static final long serialVersionUID = 8302623145428663054L;

    private Integer id;
    private String product, category, country;
    private BigDecimal amount;
    private Date date;

    /**
     * C'tor
     * 
     * @param id
     *            - trade id
     * @param product
     * @param category
     * @param country
     * @param amount
     * @param date
     */
    public Trade(final Integer id, final String product, final String category, final String country,
	    final BigDecimal amount, final Date date) {
	this.id = id;
	this.product = product;
	this.category = category;
	this.country = country;
	this.amount = amount;
	this.date = date;
    }

    /**
     * Trade ID
     * 
     * @return trade id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Product of trade
     * 
     * @return product
     */
    public String getProduct() {
	return product;
    }

    /**
     * Category of some product getter
     * 
     * @return trade category
     */
    public String getCategory() {
	return category;
    }

    /**
     * Amount of money
     * 
     * @return amount
     */
    public BigDecimal getAmount() {
	return amount;
    }

    /**
     * Trade date getter
     * 
     * @return trade date
     */
    public Date getDate() {
	return date;
    }

    /**
     * Country getter
     * 
     * @return country
     */
    public String getCountry() {
	return country;
    }

}
