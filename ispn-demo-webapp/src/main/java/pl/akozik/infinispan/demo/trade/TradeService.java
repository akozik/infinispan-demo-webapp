package pl.akozik.infinispan.demo.trade;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Trade service provides a list of trades from CSV file.
 * 
 * @author Anton Kozik
 * 
 */
public class TradeService implements Serializable {

    private static final long serialVersionUID = 1324121515474511999L;

    private static final String PART = "trade.part";

    /**
     * C'tor
     */
    public TradeService() {
	// intentionally left blank
    }

    /**
     * Provides initial list of trades
     * 
     * @return list of trades
     */
    public List<Trade> getTrades() {
	final String[] files = getFiles();
	final List<Trade> trades = parseTrades(files);
	return trades;
    }

    private String[] getFiles() {
	final String part = System.getProperty(PART, "default");
	String[] files;
	switch (part) {
	case "one":
	    files = new String[] { "trade1.csv" };
	    break;
	case "two":
	    files = new String[] { "trade2.csv" };
	    break;
	default:
	    files = new String[] { "trade1.csv", "trade2.csv" };
	    break;
	}
	return files;
    }

    private List<Trade> parseTrades(final String[] files) {
	List<Trade> trades = new ArrayList<>();
	for (final String file : files) {
	    final Reader inputReader = new InputStreamReader(getClass().getResourceAsStream("/" + file));
	    try (final CSVReader csv = new CSVReader(inputReader, ';', '"', 1)) {
		final List<String[]> lines = csv.readAll();
		for (final String[] line : lines) {
		    trades.add(buildTrade(line));
		}
	    } catch (final IOException | ParseException e) {
		e.printStackTrace();
	    }
	}
	return trades;
    }

    private Trade buildTrade(final String[] line) throws ParseException {
	final Integer id = Integer.parseInt(line[0]);
	final String product = line[1];
	final String category = line[2];
	final Number number = NumberFormat.getNumberInstance(Locale.FRANCE).parse(line[3].substring(1));
	BigDecimal amount;
	if (number instanceof Double) {
	    amount = new BigDecimal((Double) number);
	} else if (number instanceof Long) {
	    amount = new BigDecimal((Long) number);
	} else {
	    amount = BigDecimal.ZERO;
	}
	final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	final Date date = dateFormat.parse(line[4]);
	final String country = line[5];
	return new Trade(id, product, category, country, amount, date);
    }

}
