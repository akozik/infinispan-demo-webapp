package pl.akozik.infinispan.demo;

import static java.lang.String.format;

import java.util.List;

import org.infinispan.Cache;

import pl.akozik.infinispan.demo.cache.CacheManager;
import pl.akozik.infinispan.demo.trade.Trade;
import pl.akozik.infinispan.demo.trade.TradeService;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

/**
 * 
 * Add button click action implementation.
 * 
 * @author Anton Kozik
 * 
 */
class AddButtonClickListener implements Button.ClickListener {

    private static final long serialVersionUID = -4618880354694853357L;

    private static final String NOTIFICATION_FORMAT = "Objects are successfully loaded into the cache! It took %d ms";

    private TradeService tradeService = new TradeService();

    /**
     * Get initial trades from CSV file and put it into the cache.
     */
    @Override
    public void buttonClick(final ClickEvent event) {
	final Cache<Integer, Trade> cache = CacheManager.getInstance().getCache(CacheManager.CACHE_NAME);
	final List<Trade> trades = tradeService.getTrades();

	final long start = System.nanoTime();
	for (final Trade trade : trades) {
	    cache.put(trade.getId(), trade);
	}
	final long finish = System.nanoTime();

	Notification.show(format(NOTIFICATION_FORMAT, (finish - start) / 1_000_000));
	updateNumberOfEntries(cache.size());
	updateDataTable(trades);
    }

    /**
     * Updates number of entries in cache.
     * 
     * @param cacheSize
     *            - number of keys in cache
     */
    protected void updateNumberOfEntries(final int cacheSize) {
	((InfinispanDemoUI) UI.getCurrent()).updateNumberOfEntriesLabel(cacheSize);
    }

    /**
     * Update table of trade objects on page.
     * 
     * @param trades
     *            - list of trade for table
     */
    protected void updateDataTable(final List<Trade> trades) {
	((InfinispanDemoUI) UI.getCurrent()).getDataTable().addItems(trades);
    }

}
