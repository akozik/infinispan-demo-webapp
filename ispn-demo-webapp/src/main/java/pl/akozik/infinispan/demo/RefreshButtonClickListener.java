package pl.akozik.infinispan.demo;

import static pl.akozik.infinispan.demo.cache.CacheManager.CACHE_NAME;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;

import pl.akozik.infinispan.demo.cache.CacheManager;
import pl.akozik.infinispan.demo.trade.Trade;

import com.vaadin.ui.Button.ClickEvent;

/**
 * 
 * Refresh button click action listener
 * 
 * @author Anton Kozik
 * 
 */
class RefreshButtonClickListener extends AddButtonClickListener {

    private static final long serialVersionUID = 8736472590952324420L;

    /**
     * Projects cache data onto page
     */
    @Override
    public void buttonClick(final ClickEvent event) {
	final EmbeddedCacheManager cacheManager = CacheManager.getInstance();
	final Cache<Integer, Trade> cache = cacheManager.getCache(CACHE_NAME);
	final List<Trade> allTrades = new ArrayList<>();
	for (final Entry<Integer, Trade> entry : cache.entrySet()) {
	    allTrades.add(entry.getValue());
	}
	updateNumberOfEntries(cache.size());
	updateDataTable(allTrades);
    }
}
