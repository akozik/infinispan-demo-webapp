package pl.akozik.infinispan.demo.cache;

import java.io.IOException;

import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;

/**
 * 
 * Utility class that help in providing valid Infinispan Cache Manager to the
 * client code.
 * 
 * @author Anton Kozik
 * 
 */
public class CacheManager {

    public static final String CACHE_NAME = "TradeCache";

    private static EmbeddedCacheManager manager;

    /**
     * Initializes Cache Manager using configuration file. Returns Manager if
     * already initialized.
     * 
     * @return instance of embedded cache manager
     */
    public synchronized static EmbeddedCacheManager getInstance() {
	if (manager == null) {
	    try {
		manager = new DefaultCacheManager("infinispan.xml");
	    } catch (final IOException e) {
		e.printStackTrace();
	    }
	}
	return manager;
    }

    /**
     * Closes cache manager.
     */
    public static void stop() {
	manager.stop();
    }

    private CacheManager() {
	// intentionally left blank
    }

}
